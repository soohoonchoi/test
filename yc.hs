Y :: (c -> c) -> c
Y c = c (Y c)
